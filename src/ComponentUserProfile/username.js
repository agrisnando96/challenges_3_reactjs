import React from 'react';

const Username = (props) => {
    return (
        <div>
            {props.name}
        </div>
    );
};

Username.defaultProps = {
      name: "User"
    };

export default Username;