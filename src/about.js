import React from "react";
import logo from './logo.svg';
import './App.css';


class About extends React.Component {
    render = () => (
        <div className="container mt-2">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h1>{this.props.match.params.number}</h1>
                <p>
                    Orang nya suka menabung
                </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
        </a>
            </header>
        </div>
    );
}
export default About;