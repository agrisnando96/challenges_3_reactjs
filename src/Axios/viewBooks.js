import React from 'react';

import Axios from 'axios';

export default class ViewBooks extends React.Component {
    state = {
        books: []
    };


    componentDidMount() {
        Axios.get('http://127.0.0.1:3000/books/').then(res => {
            const books = res.data;
            this.setState({ books: books.data });
        });
    }

    deleteBooks = id =>{
        Axios
        .delete(`http://127.0.0.1:3000/books/${id}`)
        .then(res =>{
            console.log(res);
            console.log(res);
            alert(res.data.message);
        })
        .catch(err =>{
            console.log(err);
        })
        .finally(()=>{
            this.componentDidMount();
        })
    }

    render() {
        return (
            <div className="container mt-5">
                <h1>Data Semua Buku</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Tittle</th>
                            <th scope="col">Author</th>
                            <th scope="col">Published_date</th>
                            <th scope="col">Pages</th>
                            <th scope="col">Leanguage</th>
                            <th scope="col">Publisher_id</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.books.map(books => (
                            <tr key={books.id}>
                                <th scope="row">{books.id}</th>
                                <td>{books.title}</td>
                                <td>{books.author}</td>
                                <td>{books.published_date}</td>
                                <td>{books.pages}</td>
                                <td>{books.language}</td>
                                <td>{books.publisher_id}</td>
                                <td><a href={"http://localhost:3001/editBooks/"+books.id}>Edit </a> 
                                 | <a onClick={() => this.deleteBooks(books.id)} className="btn btn-info">Delete</a></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}
