// Render Prop
import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import Axios from 'axios';


const AddBooks = () => (
    <div>
        <h1><center>Tambah Data Buku</center></h1>
        <Formik
            initialValues={{ title: '', author: '', published_date: '', pages: '', language: '', publisher_id: '' }}
            validate={values => {
                const errors = {};
                if (!values.title) {
                    errors.title = 'judul harus di isi';
                } else if (!values.author) {
                    errors.author = 'required';
                } else if (!values.published_date) {
                    errors.published_date = 'required';
                } else if (!values.pages) {
                    errors.pages = 'required';
                } else if (!values.language) {
                    errors.leanguage = 'required';
                } else if (!values.publisher_id) {
                    errors.publisher_id = 'required';
                }
                return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    Axios.post('http://127.0.0.1:3000/books', values)
                        .then(res => {
                            console.log(res);
                            console.log(res);
                            alert(res.data.message);
                        })
                        .catch(err => {
                            console.log(err);
                        })
                    setSubmitting(false);
                }, 400);
            }}
        >
            {({ values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting }) => (
                    <Form onSubmit={handleSubmit}>
                        <div className="App">
                            <div className="container">
                                <div className="form-group container col-5">
                                    <label>Title</label>
                                    <Field type="text" className="form-control" name="title" />
                                    <ErrorMessage name="title" component="div" />
                                    {errors.email && touched.email && errors.email}
                                </div>

                                <div className="form-group container col-5">
                                    <label>Authors</label>
                                    <Field type="text" className="form-control" name="author" />
                                    <ErrorMessage name="author" component="div" />
                                </div>

                                <div className="form-group container col-5">
                                    <label>Publish_date</label>
                                    < Field type="date" className="form-control" name="published_date" />
                                    <ErrorMessage name="published_date" component="div" />
                                </div>

                                <div className="form-group container col-5">
                                    <label>Pages</label>
                                    <Field type="text" className="form-control" name="pages" />
                                    <ErrorMessage name="pages" component="div" />
                                </div>

                                <div className="form-group container col-5">
                                    <label>Leanguage</label>
                                    <Field type="text" className="form-control" name="language" />
                                    <ErrorMessage name="language" component="div" />
                                </div>

                                <div className="form-group container col-5">
                                    <label>Publisher_id</label>
                                    <Field type="text" className="form-control" name="publisher_id" />
                                    <ErrorMessage name="publisher_id" component="div" />
                                </div>

                                <div className="form-group container col-5">
                                    <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Submit</button>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
        </Formik>
    </div>
);

export default AddBooks;
