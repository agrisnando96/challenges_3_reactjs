import React from 'react';

import Axios from 'axios';


export default class DeleteBooks extends React.Component {
        state = {
          id: '',
        }
      
        deleteBooks = id =>{
            Axios
            .delete(`http://127.0.0.1:3000/books/${id}`)
            .then(res =>{
                console.log(res);
                console.log(res);
                alert(res.data.message);
            })
            .catch(err =>{
                console.log(err);
            })
            .finally(()=>{
                this.componentDidMount();
            })
        }    
    }
