// import React from 'react';
// import { Formik, Form, Field, ErrorMessage } from 'formik';
// import Axios from 'axios';
// import { useFormik } from 'formik';

// const EditBooks = () => {
//     // Pass the useFormik() hook initial form values and a submit function that will
//     // be called when the form is submitted
//     const formik = useFormik({
//         initialValues: {
//             title: '',
//             author: '',
//             published_date: '',
//             pages: '',
//             language: '',
//             publisher_id: ''
//         },
//         onSubmit: values => {
//             alert(JSON.stringify(values, null, 2));
//         },
//     });
//     return (
//         <form onSubmit={formik.handleSubmit}>
//             <label htmlFor="title">Title</label>
//             <input
//                 id="title"
//                 name="title"
//                 type="title"
//                 onChange={formik.handleChange}
//                 value={formik.values.title}
//             />

//             <label htmlFor="author">Author</label>
//             <input
//                 id="author"
//                 name="author"
//                 type="author"
//                 onChange={formik.handleChange}
//                 value={formik.values.author}
//             />

//             <label htmlFor=" published_date"> published_date</label>
//             <input
//                 id=" published_date"
//                 name=" published_date"
//                 type=" published_date"
//                 onChange={formik.handleChange}
//                 value={formik.values. published_date}
//             />

//             <label htmlFor="pages">pages</label>
//             <input
//                 id="pages"
//                 name="pages"
//                 type="pages"
//                 onChange={formik.handleChange}
//                 value={formik.values.pages}
//             />

//             <label htmlFor="language">language</label>
//             <input
//                 id="language"
//                 name="language"
//                 type="language"
//                 onChange={formik.handleChange}
//                 value={formik.values.language}
//             />

//             <label htmlFor="publisher_id">publisher_id</label>
//             <input
//                 id="publisher_id"
//                 name="publisher_id"
//                 type="publisher_id"
//                 onChange={formik.handleChange}
//                 value={formik.values.publisher_id}
//             />
//             <button type="submit">Submit</button>
//         </form>
//     );
// };


// export default EditBooks;