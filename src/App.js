import React from 'react';
// import logo from './logo.svg';
import './App.css';
import './Axios/PersonList';
import PersonList from './Axios/PersonList';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Selamat Datang di Website Kami
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React

        </a>
        <PersonList />
      </header>
    </div>
  );
}

export default App;
