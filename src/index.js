import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import App from './App';
import About from './about';
import Profile from './profile';
import Notfound from './notfound';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import NameForm from './form';
import MultiInputForm  from './multiform';
import ValidasiLogin from './ValidasiLogin';
import Login from './Login';
import Example from './Hooks';
import PersonList from './Axios/PersonList';
import ViewBooks from './Axios/viewBooks';
import AddBooks from './Axios/addBooks';
import Book from './Axios/books';



const routing = (
    <Router>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <Link className="navbar-brand" to="/">
                Nav
    </Link>
            <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <Link className="navbar-brand" to="/">
                            Home
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/profile">
                            Profile
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/about">
                            About
                        </Link>
                    </li>
                    {/* <li className="nav-item">
                        <Link className="navbar-brand" to="/form">
                            Form
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/multiform">
                            Multiform Input
                        </Link>
                    </li> */}
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/ValidasiLogin">
                            Sign Up
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/Login">
                            Sign in
                        </Link>
                    </li>
                    {/* <li className="nav-item">
                        <Link className="navbar-brand" to="/Hooks">
                            Ini buat belajar hooks
                        </Link>
                    </li> */}
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/PersonList">
                            Persons
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/viewBooks">
                            View Books
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/addBooks">
                            Add Books
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className="navbar-brand" to="/books">
                            Kelola Buku 
                        </Link>
                    </li>
                </ul>
            </div>
        </nav>
        <Switch>
            <Route exact path="/" component={App} />
            <Route path="/profile" component={Profile} />
            <Route path="/about/:number?" component={About} />
            <Route path="/form" component={NameForm} />
            <Route path="/multiform" component={MultiInputForm} />
            <Route path="/ValidasiLogin" component={ValidasiLogin} />
            <Route path="/Login" component={Login} />
            <Route path="/Hooks" component={Example} />
            <Route path="/PersonList" component={PersonList} />
            <Route path="/viewBooks" component={ViewBooks} />
            <Route path="/addBooks" component={AddBooks} />
            <Route path="/books" component={Book} />
            {/* <Route path="/editBooks" component={EditBooks} /> */}
            <Route component={Notfound} />
        </Switch>
    </Router>
);
ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
