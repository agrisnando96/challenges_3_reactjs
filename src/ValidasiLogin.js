// Render Prop
import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';

const ValidasiLogin = () => (
    <div>
        <h1><center>Sign Up</center></h1>
        <Formik
            initialValues={{ name: '', email: '', password: '', repassword: '' }}
            validate={values => {
                const errors = {};
                if (!values.name) {
                    errors.name = 'nama harus di isi';
                }
                else if (!values.email) {
                    errors.email = 'Required';
                } else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                    errors.email = 'Invalid email address';
                } else if (!values.password) {
                    errors.password = 'password harus di isi';
                } else if (values.password !== values.repassword) {
                    errors.repassword = 'password tidak sama';
                }
                return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    alert(JSON.stringify(values, null, 2));
                    setSubmitting(false);
                }, 400);
            }}
        >
            {({ isSubmitting }) => (
                <Form>
                    <div className="container">
                        <div className="form-group row">
                            <label>Username</label>
                            <Field type="text" className="form-control" name="name" />
                            <ErrorMessage name="name" component="div" />
                        </div>

                        <div className="form-group row">
                            <label>Email</label>
                            <Field type="email" className="form-control" name="email" />
                            <ErrorMessage name="email" component="div" />
                        </div>

                        <div className="form-group row">
                            <label>Password</label>
                            <Field type="password" className="form-control" name="password" />
                            <ErrorMessage name="password" component="div" />
                        </div>

                        <div className="form-group row">
                            <label>Confirm Password</label>
                            <Field type="password" className="form-control" name="repassword" />
                            <ErrorMessage name="repassword" component="div" />
                        </div>

                        <div className="form-group row">
                            <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Submit</button>
                        </div>

                    </div>
                </Form>
            )}
        </Formik>
    </div>
);

export default ValidasiLogin;