import React from 'react';
import Avatar from './ComponentUserProfile/avatar';
import Biodata from './ComponentUserProfile/bio';
import Username from './ComponentUserProfile/username';
// import { CardBody, Card, Row, Col, CardSubtitle, CardTitle, CardText } from 'reactstrap'

class Profile extends React.Component {
    render = () => (
        <div className="container mt-2">
            <header className="App-header">
                <Avatar />
                <Username name = "Agris Grisnando"/>
                <Biodata />
            </header>
        </div>
    );
}
export default Profile;