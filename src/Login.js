// Render Prop
import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';

const Login = () => (
    <div>
        <h1><center>Login</center></h1>
        <Formik
            initialValues={{ email: '', password: '' }}
            validate={values => {
                const errors = {};
                if (!values.email) {
                    errors.email = 'Email nya isi dulu dongs';
                } else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                    errors.email = 'Invalid email address';
                } else if (!values.password) {
                    errors.password = 'eta password isi bray';
                }
                return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    alert(JSON.stringify(values, null, 2));
                    setSubmitting(false);
                }, 400);
            }}
        >
            {({ isSubmitting }) => (
                <Form>
                    <div className="App">
                        <div className="container">
                            <div className="form-group container col-5">
                                <label>Email</label>
                                <Field type="email" className="form-control" name="email" />
                                <ErrorMessage name="email" component="div" />
                            </div>

                            <div className="form-group container col-5">
                                <label>Password</label>
                                <Field type="password" className="form-control" name="password" />
                                <ErrorMessage name="password" component="div" />
                            </div>

                            <div className="form-group container col-5">
                                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Submit</button>
                            </div>
                        </div>
                    </div>
                </Form>
            )}
        </Formik>
    </div>
);

export default Login;